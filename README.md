# Stash Markdown Viewer Plugin

This plugin adds a 'Markdown View' button to the source view of Stash.

![screenshot](https://bitbucket.org/atlassianlabs/stash-markdown-viewer-plugin/raw/master/screenshot.png)
